package alvaro.lab2project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class StopActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stop_activity);
    }

    public void stopService(View view){
        Intent i = new Intent(this, ReadingSensorService.class);
        Log.i("My Activity", "You pressed the button Stop");
        i.putExtra("Action", "stopService");
        i.putExtra("Address", MainActivity.address);
        i.putExtra("Port", MainActivity.port);
        startService(i);

        i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }
}

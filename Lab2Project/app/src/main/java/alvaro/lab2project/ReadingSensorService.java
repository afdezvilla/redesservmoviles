package alvaro.lab2project;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ReadingSensorService extends IntentService implements SensorEventListener {

    private static SensorManager sensorManager;
    private Sensor sensor;

    private static Socket socket;
    //private static final int SERVER_PORT = 5000;
    //private static final String SERVER_IP = "10.0.2.15";
    private String address = " ";
    private int port = 0;

    public ReadingSensorService() {
        super("ReadingSensorService");
    }


    @Override
    protected void onHandleIntent(Intent i) {
        Log.i("Service", "I already start service");
        Bundle bundle = i.getExtras();
        String action = (String)bundle.get("Action");
        address = (String)bundle.get("Address");
        port = (Integer)bundle.get("Port");
        Log.i("address", address);
        Log.i("port", (Integer.toString(port)));
        if(action.equals("launchedService")) {
            sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            if (sensor.equals(null)) {
                Log.e("Service", "Error getting the sensor.");
            } else {
                sensorManager.registerListener(this, sensor, sensorManager.SENSOR_DELAY_NORMAL);
            }
            new Thread(new ClientThread()).start();
        }
        if(action.equals("stopService")){
            try {
                sensorManager.unregisterListener(this);
                stopSelf();
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float[] values = event.values;
            String x = Float.toString(values[0]);
            String y = Float.toString(values[1]);
            String z = Float.toString(values[2]);
            String str = "X Axis: "+x+" Y Axis: "+y+" Z Axis: "+z;
            Log.i("Service","Update with " +x+" "+y+" "+z);
            try{
                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())), true);
                out.println(str);
            }catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    class ClientThread implements Runnable {
        @Override
        public void run() {
            try {
                InetAddress serverAddr = InetAddress.getByName(address);
                socket = new Socket(serverAddr, port);
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

}

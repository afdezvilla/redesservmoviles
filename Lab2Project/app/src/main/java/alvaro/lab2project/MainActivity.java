package alvaro.lab2project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText txtAddress;
    private EditText txtPort;
    static String address=" ";
    static Integer port= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        txtAddress = (EditText)findViewById(R.id.txtAddress);
        txtPort = (EditText)findViewById(R.id.txtPort);
    }

    public void launchedService(View view){
        Intent i = new Intent(this, ReadingSensorService.class);
        Log.i("My Activity", "You pressed the button Start");
        address = txtAddress.getText().toString();
        i.putExtra("Action", "launchedService");
        i.putExtra("Address", address);
        port  = Integer.parseInt(txtPort.getText().toString());
        i.putExtra("Port", port);
        startService(i);
        i = new Intent(this, StopActivity.class);
        startActivity(i);
        finish();
    }
}
